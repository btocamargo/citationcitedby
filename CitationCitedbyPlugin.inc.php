<?php
/**
 * @file plugins/generic/citationCitecby/CitationCitedbyPlugin.inc.php
 *
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @btocamargo - Roberto Camargo 2019
 * 
 */
import('lib.pkp.classes.plugins.GenericPlugin');
import('plugins.importexport.crossref.CrossRefExportPlugin');


class CitationCitedbyPlugin extends GenericPlugin {
	public function register($category, $path, $mainContextId = NULL) {
		$success = parent::register($category, $path);
		if ($success && $this->getEnabled()) {
			HookRegistry::register('Templates::Article::Main', array($this, 'addCitation'), HOOK_SEQUENCE_NORMAL);
		}

		return $success;
	}

	public function getDisplayName() {
		return 'Citation Citedby';
	}

	public function getDescription() {
		return 'This plugin returns items cite in the current DOI';
	}

	public function getDOI(){
		$templateMgr =& TemplateManager::getManager();
		$article = $templateMgr->get_template_vars('article');

		return $article->getStoredPubId('doi');
	}

	function loadTitlesIntoArray($tagName, $path){
		libxml_use_internal_errors(true);
		$xml = simplexml_load_file( $path );

		if($xml === false){
			return null;
		}


		if ( $xml instanceof SimpleXMLElement ) {

			$dom = new DOMDocument( '1.0', 'utf-8' );
			$dom->preserveWhiteSpace = false;
			$dom->formatOutput = true;


			$dom->loadXML( $xml->asXML() );

			$titels = array();
			$marker = $dom->getElementsByTagName( $tagName );

			for ( $i = $marker->length - 1; $i >= 0; $i-- ) {
				$new = $marker->item( $i )->textContent;
				array_push( $titels, $new );
			}

			return  $titels;
		}
		
	}

	public function addCitation($hookName, $params) {

		$crosSettings = new CrossRefExportPlugin();

		$request = $this->getRequest();
		$context = $request->getContext();


		$usr = $crosSettings->getSetting($context->getId(), 'username');
		$pwd = $crosSettings->getSetting($context->getId(), 'password');
		$doi = $this->getDOI();

		$xmlfile = 'https://doi.crossref.org/servlet/getForwardLinks?usr='.$usr.'&pwd='.$pwd.'&doi='.$doi;

		$citationArr = $this->loadTitlesIntoArray("doi",$xmlfile);

		$citationArr == null ? '': $this->showCitation($citationArr);
	}


	public function showCitation($variable){

		echo "<div>";
		echo "<h2 style='padding-left:20px;margin-bottom:15px'>Citações</h2>";
		echo "<ul style='list-style-type: none;padding-left:20px'>";
		foreach ($variable as $value) {
			echo "<li><a href='https://doi.org/$value' target='_blank'>$value</a></li>";	
		}
		echo "</ul>";
		echo "<h3 style='padding-left:20px;margin-top:15px'>".count($variable)." artigos citam este artigo</h3>";	
		// ITEMS CITE THIS ARTICLE
		echo "</div>";
	}	

	


}